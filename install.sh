#!/bin/bash

echo "-------------------------"
echo "---> install auditbeat..."
echo "-------------------------"

rpm --import https://packages.elastic.co/GPG-KEY-elasticsearch

cat << EOF > /etc/yum.repos.d/elastic.repo
[elastic-7.x]
name=Elastic repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=0
autorefresh=1
type=rpm-md
EOF

yum -y --enablerepo=elastic* install auditbeat-7.9.2

echo "-------------------------"
echo "--> disable auditd..."
echo "-------------------------"

service auditd stop
chkconfig auditd off
systemctl mask systemd-journald-audit.socket

echo "-------------------------"
echo "---> config auditbeat..."
echo "-------------------------"

cp -f auditbeat.yml /etc/auditbeat/
cp -f logstash-ca.pem /etc/auditbeat/
cp -f rules.conf /etc/auditbeat/audit.rules.d/
cp -f auditbeat.service /usr/lib/systemd/system/
systemctl daemon-reload
systemctl enable auditbeat
